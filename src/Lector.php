<?php

namespace Drupal\factura_scripts_bridge;

use GuzzleHttp\ClientInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Class Lector.
 */

class Lector implements LectorInterface {
 
 
  /**
   * GuzzleHttp\ClientInterface definition.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;
  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new Lector object.
   */
  public function __construct(ClientInterface $http_client, ConfigFactoryInterface $config_manager) {
   
    $this->httpClient = $http_client;
    $this->configFactory = $config_manager;
  }

 
  public function llegeix() {

    return "Hola món";

  }


  /**
   *
   *
   */
  public function test() {
    $config = $this->configFactory->get('factura_scripts_bridge.facturascriptsauth');

    $server = $config->get('url');
    $key = $config->get('key');
    // Returne Server:
    //kint($server  . "  ".$key);return ;
    if (empty($config->get('url'))) return "Not defined";
    return;
    $obtenir= $this->httpClient->request('GET', $server . '/api/3', ['headers'=>['Token'=> $key,'Accept'=>'application/json']]);
    //  Returns Body:
    // kint((String)$obtenir->getBody());

    $obtenirString= json_decode($obtenir->getBody(),true);
    //$obForcedString= (String) $obtenirString;
    return $obtenirString;

  }

} 
