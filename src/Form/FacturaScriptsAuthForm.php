<?php

namespace Drupal\factura_scripts_bridge\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\factura_scripts_bridge\LectorInterface;
use GuzzleHttp\ClientInterface;

/**
 * Class FacturaScriptsAuthForm.
 */
class FacturaScriptsAuthForm extends ConfigFormBase {

  /**
   * Drupal\factura_scripts_bridge\LectorInterface definition.
   *
   * @var \Drupal\factura_scripts_bridge\LectorInterface
   */
  protected $facturaScriptsBridgeLector;
  /**
   * GuzzleHttp\ClientInterface definition.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;
  /**
   * Constructs a new FacturaScriptsAuthForm object.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
      LectorInterface $factura_scripts_bridge_lector,
    ClientInterface $http_client
    ) {
    parent::__construct($config_factory);
        $this->facturaScriptsBridgeLector = $factura_scripts_bridge_lector;
    $this->httpClient = $http_client;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
            $container->get('factura_scripts_bridge.lector'),
      $container->get('http_client')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'factura_scripts_bridge.facturascriptsauth',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'factura_scripts_auth_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('factura_scripts_bridge.facturascriptsauth');

    $server_url = (empty($config->get('url')))? "https://yourfacturascripts.com" : $config->get('url');

    $form['url'] = [
      '#type' => 'url',
      '#title' => $this->t('Factura scripts url'),
      '#description' => $this->t('insert url from factura scripts host'),
      '#default_value' => $config->get('url'),
    ];

    $form['key'] = [
      '#type' => 'password',
      '#title' => $this->t('key'),
      '#description' => $this->t('insert token from: @server/ListApiKey', ['@server'=> $server_url]),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('key'),
    ];

    $form['info'] = [
      '#markup' => "hol1"
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
		if (strlen($form_state->getValue('key')) < 5) {
			$form_state->setErrorByName('key_short', $this->t('The key is too short. Please enter a full token.'));
		}
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('factura_scripts_bridge.facturascriptsauth')
      ->set('url', $form_state->getValue('url'))
      ->set('key', $form_state->getValue('key'))
      ->save();
  }

}
